Basic extension structure
=========================

An extension starts as a subclass of :py:class:`labthings.extensions.BaseExtension`. 
Each extension is described by a single ``BaseExtension`` instance, containing any number of methods, API views, and additional hardware components. 

You will build your extension by subclassing :py:class:`labthings.extensions.BaseExtension`, and adding the class to a top-level `LABTHINGS_EXTENSIONS` list.

In order to access the currently running microscope object, use the :py:func:`labthings.find_component` function, with the argument ``"org.openflexure.microscope"``. Likewise, any new components attached by other extensions can be found using their full name, as above.

A simple extension file, with no API views but application-available methods may look like:

.. literalinclude:: ./example_extension/01_basic_structure.py


Once this extension is loaded, any other extensions will have access to your methods:

.. code-block:: python

    from labthings import find_extension

    def test_extension_method():
        # Find your extension. Returns None if it hasn't been found.
        my_found_extension = find_extension("com.myname.myextension")

        # Call a function from your extension
        if my_found_extension:
            my_found_extension.identify()
